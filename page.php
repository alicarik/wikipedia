<!DOCTYPE html>
<html>
<head>
	<title>Wikipedia</title>

	<?php
		include 'base.php';
	?>

	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>


	<?php
		include 'format1.php';
	?>


	<div id="page">

		<?php

			error_reporting(E_ALL);
			ini_set('display_errors', 1);

			$article_link = $_GET['article'];

			echo '<b>article link: </b>' . $article_link . '<br>';

			echo '<b>page.php</b>' . '<br>';

			$current_link = "http://" . $_SERVER['SERVER_NAME'];

			echo '<b>current link: </b>' . $current_link . '<br>';

			include "config.php";
			include "Article.php";

			$row_id = Article::find_row_id($article_link);

			echo '<b>row id: </b>' . $row_id . '<br>';

			$query = 'SELECT * FROM articles WHERE id = "' . $row_id . '"';

			$conn_status = mysqli_query($conn, $query);

			$row = $conn_status->fetch_assoc();

			if ($row)
			{
				echo '<div id="upper-bar">';
				echo '<a href="/history/' . $article_link . '"><div id="bar-right">History</div></a>';
				echo '<a href="/edit/'    . $article_link . '"><div id="bar-right">Edit</div></a>';
				echo '<a href="/'   . $article_link . '"><div id="bar-left">Article</div></a>';
				echo '</div>';
				echo '<div id="title">' . $row["title"] . '</div>';
				echo '<hr>';
				echo '<div id="sitesub">From Wikipedia, the free encyclopedia</div>';
				echo '<br>';
				echo '<div id="text">' . $row["text"] . '</div>';
				echo '<br>';
				echo '<div id="date">This page was created on ' . $row["reg_date"] . '</div>';
			}
			else
			{
				echo '<br><b>page does not exist</b>';
			}
		?>

	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function(){
	  //   $("#bar-history").click(function(){

			// var url = "http://localhost.wikipedia.com/history" + window.location.pathname;

			// window.location.href = url;

	  //   });
	    $("#signup_box").click(function(){
	    	window.location.href = 'signup.php';
	    });
	    $("#signin_box").click(function(){
	    	window.location.href = 'signin.php';
	    });
	    $("#signout_box").click(function(){

	    	// alert("test");

	    	$.ajax({
	            method: 'post',
	            dataType: 'json',
	            data: {
				    	action: 'user-signout'
				      },
	            url: 'routes.php',
	            success: function (data)
	        	{
	        		if(data === 'success')
	        		{
	        			window.location.href = 'index.php';
	        		}
	            }
	        });

	    });
	});
	</script>


</body>
</html>