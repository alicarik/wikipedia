

	<div id="left-block">
		<a href="/">
			<div id="logo">
				<img src="../uploads/wikipedia-logo.png" width="150px">
			</div>
		</a>
		<ul>
		  <li>Current events</li>
		  <li>Random Article</li>
		  <li>Donate to Wikipedia</li>
		</ul>
	</div>



	<div id="header">
		<div id="box">
			<?php
				session_start();
				if (!isset($_SESSION['username']))
				{
					echo '<div id="signup_box">
							Sign Up
						</div>
						<div id="separator">|</div>
						<div id="signin_box">
							Sign In
						</div>';
				}
				else
				{
					echo '<div id="username">welcome, ' . $_SESSION['username'] . '</div>' .
						'<div id="signout_box">
							Sign Out
						</div>';
				}
			?>
		</div>
	</div>