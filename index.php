<!DOCTYPE html>
<html>
<head>
<title>Wikipedia</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	</head>


	<?php
		include 'format1.php';
	?>


	<div id="page">

		<?php

			include "config.php";

			$query = 'SELECT * FROM articles WHERE is_current_version = 1';

			$conn_status = mysqli_query($conn, $query);

			while($row = $conn_status->fetch_assoc())
			{
				echo '<div class="page-box">';
					echo '<a class="article-on-main" href="' . $row['link'] . '">' . $row['title'] . '</a>';
					echo '<br>';
					$shortenedText = substr($row['text'],0,320).'...';
					echo $shortenedText;
				echo '</div>';
			}

		?>

	</div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function(){
	    $("#signup_box").click(function(){
	    	window.location.href = 'signup.php';
	    });
	    $("#signin_box").click(function(){
	    	window.location.href = 'signin.php';
	    });
	    $("#signout_box").click(function(){

	    	// alert("test");

	    	$.ajax({
	            method: 'post',
	            dataType: 'json',
	            data: {
				    	action: 'user-signout'
				      },
	            url: 'routes.php',
	            success: function (data)
	        	{
	        		if(data === 'success')
	        		{
	        			window.location.href = 'index.php';
	        		}
	            }
	        });

	    });
	});
	</script>


</body>
</html>