<?php

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	include 'config.php';
	include 'User.php';
	include 'Article.php';
	// include 'Topic.php';
	// include 'Post.php';


	if($_POST['action'] == "user-signup")
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		$email = $_POST['email'];

		if(!empty(User::check_existing_username($username)))
		{
			$existsArray = array(
					'message' => 'username exists:' . User::check_existing_username($username),
					'redirect' => 'no'
				);
			echo json_encode($existsArray);
		}
		else
		{
			echo json_encode(User::signup($username, $password, $email));
		}
	}

	if($_POST['action'] == "user-signin")
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		echo json_encode(User::signin($username, $password));
	}

	if($_POST['action'] == "user-signout")
	{
		echo json_encode(User::signout());
	}

	if($_POST['action'] == "article-history")
	{
		$article_id = $_POST['article_id'];
		echo json_encode(Article::history($article_id));
	}

	if($_POST['action'] == "article-edit")
	{
		$title = $_POST['title'];
		$edited_text = $_POST['edited_text'];
		$link = $_POST['link'];
		$version = $_POST['version'];
		$user_id = $_POST['user_id'];
		$myArray=[];
		$action1 = Article::edit($title, $edited_text, $link, $version, $user_id, User::check_existing_id($user_id));
		$action2 = Article::disable_old_version($link, $version);
		array_push($myArray,$action1,$action2);
		echo json_encode($myArray);
	}

?>