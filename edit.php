<!DOCTYPE html>
<html>
<head>
	<title>Wikipedia</title>
	
	<?php
		include 'base.php';
	?>

	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>



	<?php
		include 'format1.php';
	?>



	<div id="page">

		<?php

			error_reporting(E_ALL);
			ini_set('display_errors', 1);

			echo '<b>edit.php</b>' . "<br>";
			$article_link = $_GET['article'];
			echo '<b>article_link : </b>' . $article_link . "<br>";

			include "config.php";
			include "Article.php";
			include "User.php";


	    	if (isset($_SESSION['username']))
			{
				$user_id = User::findid($_SESSION['username']);
			}
			else
			{
				$user_id = $_SERVER['REMOTE_ADDR'];
			}

			// $user_id = isset($_SESSION['username']) ? User::findid($_SESSION['username']) : $_SERVER['REMOTE_ADDR']; <-- also 2nd way to do it

			echo '<div id="user-id" style="display:none;">' . $user_id . '</div>';



			// if($_GET['id'] != null)
			// {
			// 	$row_id = $_GET['id'];
			// }
			// else
			// {
			// 	$row_id = Article::find_row_id($article_link);
			// }

			$row_id = $_GET['id'] != null ? $_GET['id'] : Article::find_row_id($article_link);

			echo '<div id="upper-bar">';
			echo '<a href="history/' . $article_link . '"><div id="bar-right">History</div></a>';
			echo '<a href="edit/'    . $article_link . '"><div id="bar-right">Edit</div></a>';
			echo '<a href="/'   . $article_link . '"><div id="bar-left">Article</div></a>';
			echo '</div>';

			$row_id = $conn->real_escape_string($row_id);
			$query = 'SELECT * FROM articles WHERE id = "' . $row_id . '"';
			$conn_status = mysqli_query($conn, $query);
			$row = $conn_status->fetch_assoc();

			if($row)
			{
				echo '<div id="article-title" style="display:none;">' . $row["title"] . '</div>';
				echo '<div id="article-link" style="display:none;">' . $row["link"] . '</div>';
				echo '<textarea id="textarea1">' . $row["text"] . '</textarea>';
				echo '<input type="submit" id="button1" value="Submit">';	
			}
			else
			{
				echo 'stop trying to hack, bro';
			}

			$row_id = Article::find_row_id($article_link);
			$row_id = $conn->real_escape_string($row_id);
			$query = 'SELECT * FROM articles WHERE id = "' . $row_id . '"';
			$conn_status = mysqli_query($conn, $query);
			$row = $conn_status->fetch_assoc();
			echo '<div id="article-version" style="display:none;">' . $row["version"] . '</div>';

		?>

	</div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function(){

	    $("#button1").click(function(){

	    	var title = $("#article-title").text();
	    	var edited_text = $("#textarea1").val();
	    	var link = $("#article-link").text();
	    	var version = $("#article-version").text();
	    	var user_id = $("#user-id").text();

	    	// alert(title + " " + edited_text + " " + link + " " + version + " " + user_id);

			$.ajax({
		        method: 'post',
		        dataType: 'json',
		        url: 'routes.php',
		        data: {
		        	title: title,
		        	edited_text: edited_text,
		      	 	link: link,
		      	 	version: version,
		      	 	user_id: user_id,
		        	action: 'article-edit'
		        },
		        success: function (data)
		    	{
		    		alert(data);
		    		setTimeout(function () {
				    	window.location.href = '';
					}, 1000);
		        }
		    });

		});


	    $("#signup_box").click(function(){
	    	window.location.href = 'signup.php';
	    });

	    $("#signin_box").click(function(){
	    	window.location.href = 'signin.php';
	    });

	    $("#signout_box").click(function(){
			$.ajax({
	            method: 'post',
	            dataType: 'json',
	            data: {
				    	action: 'user-signout'
				      },
	            url: 'routes.php',
	            success: function (data)
	        	{
	        		if(data === 'success')
	        		{
	        			window.location.href = '';
	        		}
	            }
	        });
	    });

	});
	</script>


</body>
</html>