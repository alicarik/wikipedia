<?php

	class User
	{	

		static function findid($username)
		{
			global $conn;

			$query = 'SELECT * FROM users WHERE username = "' . $username . '"';

			$conn_status = mysqli_query($conn, $query);

			$row = $conn_status->fetch_assoc();

			if($row)
			{
				$user_id = $row['id'];
				return $user_id;
			}
		}

		static function delete($arr)
		{

			$commands=[];

			for($i=0; $i<count($arr); $i++)
			{
				// global $conn;

				$query = 'DELETE FROM users WHERE id = "' . $arr[$i] . '"';
				
				array_push($commands,$query);

				// $conn_status = mysqli_query($conn, $query);
			}

			return $commands;

		}

		static function check_existing_id($user_id)
		{
			global $conn;

			$query = 'SELECT * FROM users WHERE id = "' . $user_id . '"';

			$conn_status = mysqli_query($conn, $query);

			$row = $conn_status->fetch_assoc();

			if($row)
			{
				return $row['id'];
			}
			else
			{
				return false;
			}
		}

		static function check_existing_username($username)
		{
			global $conn;

			$query = 'SELECT * FROM users WHERE username = "' . $username . '"';

			$conn_status = mysqli_query($conn, $query);

			$row = $conn_status->fetch_assoc();

			if($row)
			{
				$existing_username = $row['username'];
				return $existing_username;
			}
		}

		static function signup($username, $password, $email)
		{

			if (!preg_match("/^[a-zA-Z0-9_]*$/",$username))
			{
				return array(
					'message' => 'username can include only letters, no spaces accepted!',
					'redirect' => 'no'
				);
				exit;
			}
			else if (!preg_match("/^[a-zA-Z0-9_]*$/",$password))
			{
				return array(
					'message' => 'password can include only letters, no spaces accepted!',
					'redirect' => 'no'
				);
				exit;
			}
			else if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			{
				return array(
					'message' => 'doesnt look like a valid email',
					'redirect' => 'no'
				);
				exit;
			}
			else
			{
				$user_type = "user";

				global $conn;

				$hashed_password = hash('sha256', $password);

				$query = 'INSERT INTO users (username, password, user_type) VALUES("' . $username . '", "' . $hashed_password . '", "' . $user_type . '")';

				$conn_status = mysqli_query($conn, $query);

				if($conn_status)
				{
					session_start();
					$_SESSION['username'] = $username;
					return array(
						'message' => 'the username ' . $username . ' has successfully registered.',
						'redirect' => 'yes'
					);
				}
			}

		}

		static function signin($username, $password)
		{
			global $conn;

			$hashed_password = hash('sha256', $password);

			$query = 'SELECT * FROM users WHERE username = "' . $username . '" AND password = "' . $hashed_password . '"';

			$conn_status = mysqli_query($conn, $query);

			$row = $conn_status->fetch_assoc();

			if($row)
			{
				session_start();
				$_SESSION['username'] = $username;
				return 'success';
			}
			else
			{
				return 'error';
			}

			// // $conn_status = mysqli_query($conn, $query);

			// // if($conn_status)
			// // {
			// // 	session_start();
			// // 	$_SESSION['username'] = $username;
			// // 	return 'the username ' . $username . ' has successfully registered.';
 			// 	}
		}

		static function signout()
		{
			// return "hi";
			// header("Location: index.php");
			session_start();
			session_destroy();
			return 'success';
		}

	}

?>