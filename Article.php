<?php

	class Article
	{

		static function find_row_id($article_link)
		{

			global $conn;

			$query = 'SELECT id FROM articles WHERE link = "' . $article_link . '" AND is_current_version = "1"';

			$conn_status = mysqli_query($conn, $query);

			$row = $conn_status->fetch_assoc();

			return $row['id'];

		}

		static function history($article_id)
		{

			global $conn;

			$query = 'SELECT 
					c.id, u.username, c.change_text, c.change_message, c.change_date
			 		FROM changes AS c
			 		JOIN users AS u
			 		ON u.id = c.user_id
			 		WHERE c.article_id = "' . $article_id . '"';

			$conn_status = mysqli_query($conn, $query);

			$myResult = "";

			$myResult .= '<table id="history-table">
					<tr>
						<th>Change ID</th>
						<th>User</th> 
						<th>Change text</th>
						<th>Change message</th> 
						<th>Change date</th>
					</tr>';

			while($row = $conn_status->fetch_assoc())
			{
				$myResult .= "<tr>
								<td>" 
									 . $row['id'] .
								"</td>
								 <td>"
									 .  $row['username'] .
								"</td>
								<td>"
									 .  $row['change_text']  .
								"</td>
								<td>"
									 . $row['change_message'] .
								"</td>
								<td>"
									 . $row['change_date'] .
								"</td>
					</tr>";
			}

			$myResult .= '</table>';

			return $myResult;
		}

		static function edit($title, $edited_text, $link, $version, $user_id, $edit_with_user)
		{
			global $conn;

			$title = $conn->real_escape_string($title);
			$edited_text = $conn->real_escape_string($edited_text);
			$link = $conn->real_escape_string($link);
			$version = $conn->real_escape_string($version);
			$user_id = $conn->real_escape_string($user_id);

			$version++;

			if ($edit_with_user) {
				$query = 'INSERT INTO articles (title, text, link, version, is_current_version, user) VALUES("'.$title.'", "'.$edited_text.'", "'.$link.'", "'.$version.'", "1", "'.$user_id.'")';
			} else {
				$query = 'INSERT INTO articles (title, text, link, version, is_current_version, ip_address) VALUES("'.$title.'", "'.$edited_text.'", "'.$link.'", "'.$version.'", "1", "'.$user_id.'")';
			}


			// return $query;

			$conn_status = mysqli_query($conn, $query);

			if($conn_status)
			{
				return "inserted new entry!";
			}
			else
			{
				return "error";
			}
		}

		static function edit_with_ip($title, $edited_text, $link, $version, $user_id)
		{
			global $conn;

			$title = $conn->real_escape_string($title);
			//$title = mysqli_real_escape_string($conn, $title);

			$edited_text = $conn->real_escape_string($edited_text);
			$link = $conn->real_escape_string($link);
			$version = $conn->real_escape_string($version);
			$user_id = $conn->real_escape_string($user_id);

			$version++;

			$query = 'INSERT INTO articles (title, text, link, version, is_current_version, ip_address) VALUES("'.$title.'", "'.$edited_text.'", "'.$link.'", "'.$version.'", "1", "'.$user_id.'")';

			// return $query;

			$conn_status = mysqli_query($conn, $query);

			if($conn_status)
			{
				return "inserted new entry!";
			}
			else
			{
				return "error";
			}
		}

		static function disable_old_version($link, $version)
		{
			global $conn;

			$query = 'UPDATE articles SET is_current_version = "0" WHERE link = "'.$link.'" AND version = "'.$version.'"';

			$conn_status = mysqli_query($conn, $query);

			if($conn_status)
			{
				return "disabled old version!";
			}
			else
			{
				return "error";
			}
		}
	}

?>