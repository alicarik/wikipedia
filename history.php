<!DOCTYPE html>
<html>
<head>
	<title>Wikipedia</title>

	<?php
		include 'base.php';
	?>

	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>



	<?php
		include 'format1.php';
	?>



	<div id="page">


		<?php

			error_reporting(E_ALL);
			ini_set('display_errors', 1);

			echo '<b>history.php</b>' . "<br>";

			$article_link = $_GET['article'];

			echo '<div id="article-link">' . $article_link . '</div>';

			echo '<div id="upper-bar">';
			echo '<a href="history/' . $article_link . '"><div id="bar-right">History</div></a>';
			echo '<a href="edit/'    . $article_link . '"><div id="bar-right">Edit</div></a>';
			echo '<a href="/'   . $article_link . '"><div id="bar-left">Article</div></a>';
			echo '</div>';

			echo "<br>";

			include "config.php";
			include "Article.php";

			$article_id = Article::find_row_id($article_link);

			echo '<div id="article-id">' . $article_id . '</div>';

			echo '<br>';

			$query = 'SELECT a.reg_date,
							 a.text,
							 a.ip_address,
							 u.username,
							 a.id
					FROM articles AS a
					LEFT JOIN users AS u
					ON u.id = a.user
					WHERE link = "' . $article_link . '"
					ORDER BY a.reg_date DESC';

			$conn_status = mysqli_query($conn, $query);

			echo '<table id="history-table">';

			// $x = 1;
			
			while($row = $conn_status->fetch_assoc())
			{
				if($row['username'])
				{
					echo "<tr>
							<td>"
								 . $row['reg_date'] .
							"</td>
							 <td>"
								 . $row['text'] .
							"</td>
							<td>"
								 . $row['username'] .
							"</td>
							<td>"
								 . '<span class="span-text" id="' . $row['id'] . '">preview</span></td>' .
							"</td>
					</tr>";

					// $x++;
				}
				else
				{
					echo "<tr>
							<td>"
								 . $row['reg_date'] .
							"</td>
							 <td>"
								 . $row['text'] .
							"</td>
							<td>"
								 . $row['ip_address'] .
							"</td>
							<td>"
								 . '<span class="span-text" id="' . $row['id'] . '">preview</span></td>' .
							"</td>
					</tr>";
				}
			}


			echo "</table>";

		?>
	</div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function(){


		$("#history-table").on('click', '.span-text', function() {

			var myArticleLink = $('#article-link').text();
			var myId = $(this).attr('id');
			// var myArticleId = $('#article-id').text();

			// alert('/edit/' + myArticleLink + "/" + myId);

			window.location.href = ('/edit/' + myArticleLink + "/" + myId);

		});


	    $("#signup_box").click(function(){
	    	window.location.href = 'signup.php';
	    });
	    $("#signin_box").click(function(){
	    	window.location.href = 'signin.php';
	    });
	    $("#signout_box").click(function(){

	    	// alert("test");
	    	$.ajax({
	            method: 'post',
	            dataType: 'json',
	            data: {
				    	action: 'user-signout'
				      },
	            url: 'routes.php',
	            success: function (data)
	        	{
	        		if(data === 'success')
	        		{
	        			window.location.href = 'index.php';
	        		}
	            }
	        });

	    });
	});
	</script>


</body>
</html>