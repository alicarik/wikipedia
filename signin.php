<!DOCTYPE html>
<html>
<head>
<title>Wikipedia</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<?php

		include 'format1.php';

		session_start();
		if ($_SESSION[username])
		{
			header("Location: index.php");
		}

	?>

	<div id="page">
		<br>
		Sign In
		<br>
		Name: <input type="text" id="username_field"><br>
		Password: <input type="text" id="password_field"><br>
		<input type="submit" id="signin_submit">
	</div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function(){

	    $("#signup_box").click(function(){
	    	window.location.href = 'signup.php';
	    });

	    $("#signin_submit").click(function(){
	    	var username = $("#username_field").val();
	    	var password = $("#password_field").val();

	        if (!username)
	        {
	            alert("username is empty");
	            return false;
	        }

	        if (!password)
	        {
	            alert("password is empty");
	            return false;
	        }

	        $.ajax({
	            method: 'post',
	            dataType: 'json',
	            data: {
	            	    username: username,
	            		password: password,
				    	action: 'user-signin'
				      },
	            url: 'routes.php',
	            success: function (data)
	        	{
	        		if (data === 'success')
	        		{
				   		window.location.href = "index.php";
	        		}
	        		else if (data === 'error')
	        		{
	        			alert(data);
	        		}
	            }
	        });

	    });
	});
	</script>


</body>
</html>