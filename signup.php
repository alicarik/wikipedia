<!DOCTYPE html>
<html>
<head>
<title>Wikipedia</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<?php

		include 'format1.php';

		session_start();
		if ($_SESSION[username])
		{
			header("Location: index.php");
		}

	?>

	<div id="page">
		<br>
		Sign Up
		<br>
		Name: <input type="text" id="username_field"><br>
		Password: <input type="text" id="password_field"><br>
		E-mail: <input type="text" id="email_field"><br>
		<input type="submit" id="signup_submit">
	</div>


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
	$(document).ready(function(){

	    $("#signup_submit").click(function(){

	    	var username = $("#username_field").val();
	    	var password = $("#password_field").val();
	    	var email = $("#email_field").val();

	        if (!username || !password || !email)
	        {
	            alert("please fill in all fields");
	            return false;
	        }

	        // alert(username + " " + password);

	        $.ajax({
	            method: 'post',
	            dataType: 'json',
	            data: {
	            	    username: username,
	            		password: password,
	            		email: email,
				    	action: 'user-signup'
				      },
	            url: 'routes.php',
	            success: function (data)
	        	{
	        		alert(data.message);
	        		if(data.redirect == "yes")
	        		{
	        	    	window.location.href = "<?php echo $main_url;?>";
	        		}
	            }
	        });

	    });

	    $("#signin_box").click(function(){
	    	window.location.href = "signin.php";
	    });
	});
	</script>


</body>
</html>